#include <iostream>

int main() {
	char n[3][3];
	std::cin >> n[0][0] >> n[0][1] >> n[0][2];
	std::cin >> n[1][0] >> n[1][1] >> n[1][2];
	std::cin >> n[2][0] >> n[2][1] >> n[2][2];
	for (int i = 0; i < 3; i++) {
		if (n[0][i] == n[1][i] && n[1][i] == n[2][i] && n[0][i] != '_') {
			std::cout << 1 << std::endl << n[0][i];
			return 1;
		}
		else if (n[i][0] == n[i][1] && n[i][1] == n[i][2] && n[i][0] != '_') {
			std::cout << 1 << std::endl << n[i][0];
			return 1;
		}
	}
	if ((n[0][0] == n[1][1] && n[1][1] == n[2][2] && n[1][1] != '_') ||
		(n[0][2] == n[1][1] && n[1][1] == n[2][0] && n[1][1] != '_')) {
		std::cout << 1 << std::endl << n[1][1];
		return 1;
	}
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			if (n[i][j] == '_') {
				std::cout << 0 << std::endl;
				return 1;
			}
		}
	}
	std::cout << 1 << std::endl << '_';
	return 1;
}