#include <iostream>

int main() {
	int n = 1;
	int prices[1] = {2020};
	int sum = 0;
	for (int i = 0; i < n; i++) {
		sum += prices[i];
	}
	if (sum > 3000) {
		sum = sum * 0.7;
	}
	else if (sum > 2000) {
		sum = sum * 0.8;
	}
	else if (sum > 1000) {
		sum = sum * 0.9;
	}
	std::cout << sum;
}