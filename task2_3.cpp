#include <iostream>
#include <iomanip>
#include <cmath>

int main() {
	int k = 1;
	double max_error = 0.00000001, error = 1;
	double pi = 0, last_pi = 0;
	int sign = 1;
	while (error > max_error) {
		last_pi = pi;
		pi += (1.0 / k) * sign;
		sign *= -1;
		k += 2;
		error = fabs(pi - last_pi);
	}
	std::cout << std::setprecision(9);
	std::cout << pi * 4;
}