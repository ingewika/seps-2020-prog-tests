#include <iostream>
#include <cmath>

int main() {
	int n = 10;
	int f[10] = {1, 2, 3, 2, 0, 5, 10, 9, 7, 3};
	double square = 0.0;
	for (int i = 0; i < n - 1; i++) {
		square += fabs(((f[i + 1] + f[i]) / 2));
	}
	std::cout << square;
}
