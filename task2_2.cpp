#include <iostream>

int main() {
	int n = 10;
	int f[10] = {1, 2, 3, 2, 0, 5, 10, 9, 7, 3};
	for (int i = 1; i < n - 1; i++) {
		if (
			(f[i - 1] < f[i] && f[i] > f[i + 1]) ||
			(f[i - 1] > f[i] && f[i] < f[i + 1])) {
			std::cout << i << " ";
		}
	}
}
