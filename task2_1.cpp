#include <iostream>

int main () {
	long f[50];
	f[0] = 1;
	f[1] = 1;
	for (int n = 2; n < 50; n++) {
		f[n] = f[n - 1] + f[n - 2];
		if (f[n] % 2 == 0) {
			std::cout << f[n] << " ";
		}
	}
}